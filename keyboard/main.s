.cpu cortex-m0
.thumb
.syntax unified

.thumb_func
.global main
.align 4
main:
    .init_gpio:
        ldr  r3, RESETS_BASE
        ldr  r2, ATOMIC_BITMASK_CLR         @ Atomic register access
        add  r2, r2, r3
        movs r1, #32                        @ Bit 5 - IO_BANK0
        str  r1, [r2]

        mov  r2, r3
        adds r2, r2, #0x08

        .reset_io:
            ldr  r3, [r2]
            ands r3, r3, r1
            cmp  r3, #0
            beq  .reset_io

        ldr  r3, RESETS_BASE
        ldr  r2, ATOMIC_BITMASK_CLR         @ Atomic register access
        add  r2, r2, r3
        movs r1, #1
        lsls r1, #8                         @ Bit 8 - PADS_BANK0
        str  r1, [r2]

        mov  r2, r3
        adds r2, r2, #0x08

        .reset_pads:
            ldr  r3, [r2]
            ands r3, r3, r1
            cmp  r3, #0
            beq  .reset_pads

    .init_ps2:
        movs r0, #15
        bl   init_keyboard

    .init_led:
        movs r0, #25                        @ GPIO25
        movs r1, #5                         @ Function 5 - SIO
        bl   GPIO_function_select

        movs r0, #25                        @ GPIO25
        bl   output_enable_pin

	.blink:
        @ movs r0, #25
        @ bl   xor_pin

        b   .blink


.align 4
RESETS_BASE:        .word 0x4000c000
ATOMIC_BITMASK_CLR: .word 0x3000
