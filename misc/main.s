.cpu cortex-m0
.thumb
.syntax unified

.thumb_func
.global main
.align 4
main:
    push {lr}
    .clk:
        bl   setup_internal_clk
        mov  r0, r0

    .init_gpio:
        bl   init_gpio
        mov  r0, r0

    .init_ps2:
        movs r0, #13
        bl   init_keyboard
        mov  r0, r0

    .init_led:
        movs r0, #25
        bl   init_pin_output
        mov  r0, r0

    .init_lcd:
        movs r0, #14                        @ DATA START
        movs r1, #10                        @ RS
        movs r2, #11                        @ RW
        movs r3, #12                        @ E

        bl   init_lcd
        mov  r0, r0

    .before_loop:
        movs r1, #0xab
        push {r0-r2}

        ldr  r0, [sp, 4]

        bl   c_func_foo

	.loop:
        @ movs r0, #255
        ldr  r0, =0x3e8
        bl   delay_ms
        movs r0, #25
        bl   xor_pin
        b    .loop

    pop {pc}


.align 4
RESETS_BASE:        .word 0x4000c000
ATOMIC_BITMASK_CLR: .word 0x3000
