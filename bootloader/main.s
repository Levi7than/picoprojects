.syntax unified

.thumb_func
.global main
main:
	.init:   
		ldr  r3, =0x4000f000		@ Resets.reset (Atomic bitmask clear)
		movs r2, #32           		@ IO_BANK0
		str  r2, [r3, #0]

		ldr  r3, =0x400140cc   		@IO_BANK0.GPIO25_CTRL
		movs r2, #5            		@Function 5 (SIO)
		str  r2, [r3, #0]

		ldr  r3, =0xd0000020   		@SIO_BASE.GPIO_OE
		ldr  r2, =0x02000000
		str  r2, [r3, #0]

	.blink:
		ldr  r3, =0xd000001c   		@SIO_BASE.GPIO_XOR
		ldr  r2, =0x02000000
		str  r2, [r3, #0]			@problem trzech cial

		ldr r0, =0x100000
		push {r0}
		bl  delay

		b   .blink


.thumb_func
delay:
	@ mov r4, r0
	pop {r4}
	.loop:
		subs r4, r4, #1
		cmp r4, #0
		bne .loop
	bx  lr
