.syntax unified

.thumb_func
.global entrypoint
entrypoint:
    ldr r0, =0x20040000             @ SRAM_STRIPED_END
    mov sp, r0
    bl boot
    b .


.thumb_func
.global boot
boot:
    ldr r0, =0x18000000             @ XIP_SSI_BASE
    movs r1, #0
    str r1, [r0, #0x08]             @ disable SSI
     
    ldr r1, =0x00000008
    str r1, [r0, #0x14]             @ set baudrate

    ldr r1, =0x001F0300
    str r1, [r0, #0]                @ set CTRL0

    ldr r1, =0x03000218
    ldr r2, =0xF4
    str r1, [r0, r2]                @ set SPI CTRL0

    movs r1, #1
    str r1, [r0, #0x08]             @ enable SSI


    ldr r0, =0x10000100             @ first address after bootloader
    ldr r1, =0x20000000             @ SRAM
    ldr r2, =0xFFF                  @ FIXME: temporary amount of bytes to be copied i.e. 4[KB]
    @                               @ the linker (?) should paste here the size of the OS

    .copy_loop:
        ldr r3,[r0]
        str r3,[r1]
        adds r0,#0x4
        adds r1,#0x4
        subs r2,#1
        bne .copy_loop

    ldr r0,=0x20000000
    mov pc, r0
