/*
@ .equ LED_PIN, 25
@ .equ PIN_DIR_IN, 0
@ .equ PIN_DIR_OUT, 1
@ .equ HIGH, 1
@ .equ LOW, 0

@ .syntax unified
@ .thumb_func

@ .global main

@ main:
@     movs r0, #0x80 // a comment
*/

.syntax unified

.thumb_func                         // This code uses Thumb instructions
.global main                        // Set entry point
main:
	.init:   
		ldr  r3, =0x4000f000  ;@ Resets.reset (Atomic bitmask clear)
		movs r2, #32          ;@ IO_BANK0
		str  r2, [r3, #0]

		ldr  r3, =0x400140cc  ;@IO_BANK0.GPIO25_CTRL
		movs r2, #5           ;@Function 5 (SIO)
		str  r2, [r3, #0]

		ldr  r3, =0xd0000020  ;@SIO_BASE.GPIO_OE
		ldr  r2, =0x02000000
		str  r2, [r3, #0]

	.blink:
		ldr  r3, =0xd000001c  ;@SIO_BASE.GPIO_XOR
		ldr  r2, =0x02000000
		str  r2, [r3, #0]

		ldr r0, =0x1000
		bl  delay

		b   .blink //previously blink


/* delay function */
.thumb_func
delay:
	mov r4, r0
	.loop:
		subs r4, r4, #1
		cmp r4, #0
		bne .loop
		bx  lr
