.cpu cortex-m0
.thumb
.syntax unified


.equ GPIO_OE, 0x20
.equ GPIO_OE_CLR, 0x28
.equ GPIO_IN, 0x04
.equ GPIO_SET, 0x14
.equ GPIO_XOR, 0x1c

.equ PADS_BANK0_PULL_UP_ENABLE, 3

.equ ALARM0_IVT_OFFSET, 0x40
.equ IO_IRQ_BANK0_OFFSET, 0x74

.equ TIMER_INTE, 0x38
.equ TIMER_INTR, 0x34
.equ TIMER_TIMERAWL_OFFSET, 0x28
.equ TIMER_ALARM0_OFFSET, 0x10


.thumb_func
.global main
.align 4
main:
    .init_gpio:
        ldr  r3, RESETS_BASE
        ldr  r2, ATOMIC_BITMASK_CLR         @ Atomic register access
        add  r2, r2, r3
        movs r1, #32                        @ Bit 5 - IO_BANK0
        str  r1, [r2]

        movs r1, #1
        lsls r1, #8                         @ Bit 8 - PADS_BANK0
        str  r1, [r2]


    .init_watchdog:
        ldr  r3, WATCHDOG_TICK
        movs r2, #1
        lsls r2, r2, #9                 @ watchdog tick enable bits
        movs r4, #12                    @ cycles = 12 [MHz]
        orrs r2, r2, r4                 @ cycles | WATCHDOG_TICK_ENABLE_BITS
        str  r2, [r3]

    .init_alarm_interrupt:
        @@@@ set alarm interrupt subroutine for future use of delay function @@@@
        bl   set_alarm0_isr

    .init_pin_interrupt:
        @@@@ set GPIO15 to wait for interrupt @@@@

        @ 1) GPIO INIT
        movs r0, #15
        movs r1, #5                         @ 5 - SIO
        bl   GPIO_function_select

        @ 2) CONFIGURATION
        ldr  r0, PADS_BANK0_BASE
        adds r0, #0x40                      @ GPIO_15
        movs r1, #0xC8                      @ OUTPUT DISABLE + INPUT ENABLE + PULL UP + (SCHMITT)

        @ 3) GPIO_SET_IRQ_ENABLED
        @ 3.1) acknowledge_irq - "clear stale events which might cause immediate spurious handler entry"
        ldr  r0, IO_BANK0_BASE
        adds r0, #0xf4                      @ GPIO15 is in INTR1
        movs r1, #1
        lsls r1, r1, #31                    @ Bit 31 GPIO15_EDGE_HIGH - irq at raising edge
        str  r1, [r0]                       @ clear by setting INTR

        @ 3.2) set irq enabled
        ldr  r0, IO_BANK0_BASE
        ldr  r1, PROC0_INTE1_OFFSET
        add  r0, r0, r1
        movs r1, #1
        lsls r1, r1, #31                    @ Bit 31 GPIO15_EDGE_HIGH - irq at raising edge
        str  r1, [r0]                       @ set irq enabled - INTE

        @ 4) set interrupt subroutine
        bl   set_pin_interrupt_isr

    .init_led:
        movs r0, #25                        @ GPIO25
        movs r1, #5                         @ Function 5 - SIO
        bl   GPIO_function_select

        movs r0, #25                        @ GPIO25
        bl   output_enable_pin

	.blink:
        movs r0, #25
        bl   xor_pin

        ldr  r0, SECOND
        bl   delay

	b   .blink


/**
 * Connects selected GPIO pin to selected peripherial by using GPIO CTRL register.
 * r0 - GPIO Pin
 * r1 - GPIO Function (cf. RP2040 Datasheet 1.4.3)
 * */
.thumb_func
.global GPIO_function_select
.align 4
GPIO_function_select:
    ldr  r3, IO_BANK0_BASE

    movs r2, #8
    muls r2, r2, r0                 @ calculate offset for GPIO_N_CTRL (minus 0x04)
    adds r2, #0x04                  @ GPIO0_CTRL offset
    add  r3, r3, r2                 @ add calculated offset

    str  r1, [r3]                   @ write specfied function

    bx lr


/**
 * Enables output to the pin specified in r0.
 * */
.thumb_func
.global output_enable_pin
.align 4
output_enable_pin:
    ldr  r1, SIO_BASE
    adds r1, r1, GPIO_OE        @ add output enable offset
    movs r2, #1
    lsls r2, r2, r0             @ set enable on n-th pin
    str  r2, [r1]

    bx lr


/**
 * Clears output enable on the pin specified in r0, by setting it to input.
 * */
.thumb_func
.global clear_output_enable_pin
.align 4
clear_output_enable_pin:
    ldr  r1, SIO_BASE
    adds r1, r1, GPIO_OE_CLR    @ clear output enable
    movs r2, #0
    lsls r2, r2, r0             @ clear enable on n-th pin
    str  r2, [r1]

    bx lr


/**
 * Performs XOR on the value of the pin specified in r0.
 * */
.thumb_func
.global xor_pin
.align 4
xor_pin:
    ldr  r1, SIO_BASE
    adds r1, r1, GPIO_XOR
    movs r2, #1                 @ XOR value => 1 ^ 1 = 0 ; 0 ^ 1 = 1
    lsls r2, r2, r0             @ set xor on n-th pin
    str  r2, [r1]

    bx lr


/**
 * Delays by the amount of microseconds specified in r0.
 * */
.thumb_func
.global delay
.align 4
delay:
    ldr  r3, TIMER_BASE
    adds r3, r3, TIMER_INTE
    movs r2, #1                                 @ enable timer interrupt for alarm0 i.e. (1 << 0)
    str  r2, [r3]

    ldr  r3, TIMER_BASE
    ldr  r1, [r3, TIMER_TIMERAWL_OFFSET]        @ get TIMERAWL
    add  r1, r1, r0                             @ TIMERAWL + desired alarm
    str  r1, [r3, TIMER_ALARM0_OFFSET]

    @ one can use either the delay loop + alarm_fired flag
    @ or cpsid + wfi, if the amount of delay is critical before performing any other action
    ldr  r3, =alarm_fired
    movs r2, #0                                 @ ensure alarm_fired flag is false
    strb r2, [r3]

    .delay_loop:
        ldrb r2, [r3]
        cmp  r2, #1
        bne  .delay_loop

    bx  lr


/**
 * Writes alarm0 interrupt handler to the IVT.
 * */
.thumb_func
.align 4
set_alarm0_isr:
    ldr  r2, PPB_BASE
    ldr  r1, VTOR_OFFSET
    add  r2, r2, r1
    ldr  r1, [r2]                   @ read the address of IVT from VTOR hardware register

    movs r2, ALARM0_IVT_OFFSET
    add  r2, r2, r1
    ldr  r0, =alarm0_handler
    str  r0, [r2]                   @ write interrupt handler

    @@@@ enable appropriate interrupt at the CPU by turning off and on again @@@@

    movs r0, #1                     @ alarm 0 is irq0
    ldr  r2, PPB_BASE
    ldr  r1, NVIC_ICPR_OFFSET       @ unset IRQ
    add  r1, r2
    str  r0, [r1]
    ldr  r1, NVIC_ISER_OFFSET       @ set IRQ
    add  r1, r2
    str  r0, [r1]

    bx   lr


.thumb_func
.align 4
alarm0_handler:
    ldr  r3, TIMER_BASE
    adds r3, r3, TIMER_INTR
    movs r2, #1
    str  r2, [r3]                   @ reset interrupt for alarm0

    ldr  r3, =alarm_fired
    movs r2, #1                     @ alarm_fired = True
    strb r2, [r3]

    bx  lr


/**
 *
 * */
.thumb_func
.align 4
set_pin_interrupt_isr:
    ldr  r2, PPB_BASE
    ldr  r1, VTOR_OFFSET
    add  r2, r2, r1
    ldr  r1, [r2]                   @ read the address of IVT from VTOR hardware register

    movs r2, IO_IRQ_BANK0_OFFSET
    add  r2, r2, r1
    ldr  r0, =pin_handler
    str  r0, [r2]                   @ write interrupt handler

    @@@@ enable appropriate interrupt at the CPU by turning off and on again @@@@

    movs r0, #13                    @ IO_IRQ_BANK0 is 13th irq
    ldr  r2, PPB_BASE
    ldr  r1, NVIC_ICPR_OFFSET       @ unset IRQ
    add  r1, r2
    str  r0, [r1]
    ldr  r1, NVIC_ISER_OFFSET       @ set IRQ
    add  r1, r2
    str  r0, [r1]

    bx   lr


.thumb_func
.align 4
pin_handler:
    push {lr}
    ldr  r0, IO_BANK0_BASE
    adds r0, #0xf4                              @ GPIO15 is in INTR1
    movs r1, #1
    lsls r1, r1, #31                            @ Bit 31 GPIO15_EDGE_HIGH - irq at raising edge
    str  r1, [r0]                               @ clear by setting INTR

    @ do some stuff
    movs r0, #25
    bl   xor_pin

    pop {pc}


.align 4
RESETS_BASE:        .word 0x4000c000
ATOMIC_BITMASK_CLR: .word 0x3000
WATCHDOG_TICK:      .word 0x4005802c
IO_BANK0_BASE:      .word 0x40014000
PADS_BANK0_BASE:    .word 0x4001c000
PROC0_INTE1_OFFSET: .word 0x104
SIO_BASE:           .word 0xd0000000
TIMER_BASE:         .word 0x40054000
PPB_BASE:           .word 0xe0000000
VTOR_OFFSET:        .word 0xed08
NVIC_ICPR_OFFSET:   .word 0xe280        @ interrupt clear-pending register
NVIC_ISER_OFFSET:   .word 0xe100        @ interrupt set-enable register
SECOND:             .word 0xf4240

.align 4
alarm_fired:        .byte 0

/**
To enable an alarm:
 - Enable the interrupt at the timer with a write to the appropriate alarm bit in INTE: i.e. (1 << 0) for ALARM0
 - Enable the appropriate timer interrupt at the processor (see Section 2.3.2)
 - Write the time you would like the interrupt to fire to ALARM0 (i.e. the current value in TIMERAWL plus your desired
   alarm time in microseconds). Writing the time to the ALARM register sets the ARMED bit as a side effect.
 ---
 - Once the alarm has fired, the ARMED bit will be set to 0. To clear the latched interrupt, write a 1 to the appropriate bit in
   INTR.
*/
