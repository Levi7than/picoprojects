.cpu cortex-m0
.thumb
.syntax unified

.thumb_func
.global main
.align 4
main:
    .clk:
        bl   setup_internal_clk
        bl   set_alarm0_isr

    .init_gpio:
        ldr  r3, RESETS_BASE
        ldr  r2, ATOMIC_BITMASK_CLR         @ Atomic register access
        add  r2, r2, r3
        movs r1, #32                        @ Bit 5 - IO_BANK0
        str  r1, [r2]

        mov  r2, r3
        adds r2, r2, #0x08

        .reset_io:
            ldr  r3, [r2]
            ands r3, r3, r1
            cmp  r3, #0
            beq  .reset_io

        ldr  r3, RESETS_BASE
        ldr  r2, ATOMIC_BITMASK_CLR         @ Atomic register access
        add  r2, r2, r3
        movs r1, #1
        lsls r1, #8                         @ Bit 8 - PADS_BANK0
        str  r1, [r2]

        mov  r2, r3
        adds r2, r2, #0x08

        .reset_pads:
            ldr  r3, [r2]
            ands r3, r3, r1
            cmp  r3, #0
            beq  .reset_pads

    .init_ps2:
        movs r0, #13
        bl   init_keyboard

    .init_led:
        movs r0, #25
        bl   init_pin_output

    .init_lcd:
        movs r0, #14                        @ DATA START
        movs r1, #10                        @ RS
        movs r2, #11                        @ RW
        movs r3, #12                        @ E

        bl   init_lcd

	.loop:
        movs r0, #255
        @ ldr  r0, =0x3e8
        bl   delay_ms
        movs r0, #25
        bl   xor_pin
        b    .loop


.align 4
RESETS_BASE:        .word 0x4000c000
ATOMIC_BITMASK_CLR: .word 0x3000
