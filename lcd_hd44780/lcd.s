.cpu cortex-m0
.thumb
.syntax unified

.equ LCD_INIT_COMMAND, 0b00111000
.equ LCD_DISPLAY_ON,   0b00001111
.equ LCD_ENTRY_MODE,   0b00000110
.equ LCD_WRITE_CHAR,   0b01000000
.equ LCD_DISPLAY_CLR,  0b00000001

.equ GPIO_OUT_SET, 0x14
.equ GPIO_OUT_CLR, 0x18


/**
 * Initializes LCD
 * r0 - data pin start (following 7 bits will be taken by LCD data pins)
 * r1 - RS pin
 * r2 - RW pin
 * r3 - E(nable) pin
 * */
.thumb_func
.global init_lcd
.align 4
init_lcd:
    push {r4-r7, lr}
    .save_pins:
        ldr  r4, =DB0_pin
        str  r0, [r4]

        ldr  r4, =RS_pin
        str  r1, [r4]

        ldr  r4, =RW_pin
        str  r2, [r4]

        ldr  r4, =E_pin
        str  r3, [r4]

    .init_pins:
        ldr  r0, RS_pin
        bl   init_pin_output

        ldr  r0, RW_pin
        bl   init_pin_output

        ldr  r0, E_pin
        bl   init_pin_output
        ldr  r0, E_pin
        bl   clr_pin

        ldr  r4, DB0_pin                @ current pin
        movs r5, #0                     @ loop counter????

        .init_pins_loop:
            cmp  r5, #8
            beq  .init_screen

            mov  r0, r4
            bl   init_pin_output

            adds r4, r4, #1            @ add pin offset
            adds r5, r5, #1
            b    .init_pins_loop

    .init_screen:
        ldr  r0, RS_pin
        bl   clr_pin

        ldr  r0, RW_pin
        bl   clr_pin

        @ LCD INIT

        movs r0, LCD_INIT_COMMAND
        bl   lcd_send_command

        @ here the data is latched and wait 5[ms] (falling edge)
        movs r0, #5
        bl   delay_ms

        @ DISPLAY ON

        movs r0, LCD_DISPLAY_ON
        bl   lcd_send_command

        @ ENTRY MODE

        movs r0, LCD_ENTRY_MODE
        bl   lcd_send_command

        @ CLEAR DISPLAY
        movs r0, #1
        bl   delay_ms

        movs r0, LCD_DISPLAY_CLR
        bl   lcd_send_command

    pop  {r4-r7, pc}


/**
 * Prints ASCII character in r0 into the LCD screen
 * */
.thumb_func
.global lcd_write
.align 4
lcd_write:
    push {r4, lr}
    movs r4, r0

    ldr  r0, RS_pin
    bl   set_pin

    ldr  r0, RW_pin
    bl   clr_pin

    movs r0, r4
    bl   lcd_send_command

    pop  {r4, pc}

/**
 * Sends command specified in r0 into the LCD.
 * */
.thumb_func
.global lcd_send_command
.align 4
lcd_send_command:
    push {r4-r7, lr}

    movs r4, r0                 @ save LCD command

    @ 1) set true bits
    ldr  r5, DB0_pin
    lsls r4, r4, r5             @ set command at data bits position

    ldr  r2, SIO_BASE
    adds r2, r2, GPIO_OUT_SET
    str  r4, [r2]

    @ 2) Clear false bits
    mvns r0, r0                 @ negate
    ldr  r1, COMMAND_MASK
    ands r0, r0, r1
    lsls r0, r0, r5

    ldr  r2, SIO_BASE
    adds r2, r2, GPIO_OUT_CLR
    str  r0, [r2]

    @ 3) Send data to the LCD
    bl   toggle_enable_pin

    pop  {r4-r7, pc}


.align 4
IO_BANK0_BASE:      .word 0x40014000
PADS_BANK0_BASE:    .word 0x4001c000
SIO_BASE:           .word 0xd0000000
COMMAND_MASK:       .word 0x000000ff

.thumb_func
.align 4
toggle_enable_pin:
    push {lr}

    ldr  r0, E_pin
    bl   clr_pin

    ldr  r0, E_pin
    bl   set_pin

    ldr  r0, E_pin
    bl   clr_pin

    pop  {pc}

.align 4
DB0_pin:            .word 0
RS_pin:             .word 0
RW_pin:             .word 0
E_pin:              .word 0
